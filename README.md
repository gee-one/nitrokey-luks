# Using a Nitrokey/Yubikey with luks with fallback to dropbear/askpass

This repo has some hints on how to use the PGP key on a Nitrokey or Yubikey to unlock a luks partition.  These notes are tested on Bookworm/Debian 12.

If the Nitrokey/Yubikey isn't present, the script has a fallback and will allow unlocking through dropbear/ssh/local password.

I think that the recent versions of grub can handle full disk encryption (FDE) without using an unecrypted boot partition.  This is not covered here.


## Related works

Purism

https://source.puri.sm/pureos/packages/smartcard-key-luks


Cryptsetup

https://gitlab.com/cryptsetup/cryptsetup


# Initial Steps

Set up a Nitrokey/Yubikey/smartcard with your signing key.

https://docs.nitrokey.com/pro/openpgp.html

https://support.yubico.com/hc/en-us/articles/360013790259-Using-Your-YubiKey-with-OpenPGP

This is not trivial and you will have to choose if you want to either a) set up the keys on the device itself, or b) use gpg to create the keys and move them to the Nitrokey.

Either way, you should export the public key to a file so that you can manipulate it with gpg.  This "guide" assumes it is called public_nitro.asc.  If you want to use multiple keys, you will need a public key for each of them.


You will probably need some packages:
```
apt update
apt install nitrokey-app nitrocli gpa scdaemon pcscd libccid pinentry-tty
```

pinentry-curses doesn't seem to work well, but pinentry-tty seems OK for Buster, Bullseye, and Sid.

This "guide" also assumes that you already have encrypted your drive and can boot and decrypt it.


## set up environment variables

```
export TMPDIR=`mktemp --tmpdir=/dev/shm --directory`
export TMPKEY="${TMPDIR}/tmpkey"
export GNUPGHOME="${TMPDIR}"
export CRYPTHOME="/etc/cryptsetup-initramfs"
```

## import keys
`gpg --homedir ${GNUPGHOME} --import pubkey_nitro.asc`

You might have to set the trust level to ultimate.
```
gpg --homedir ${GNUPGHOME} --edit-key 12345678DEADBEEF
trust 5
```

Either of these might show you the key ID
```
gpg --homedir ${GNUPGHOME} --with-colons --list-keys  | sed -nr 's/^pub(:[^:]*){3}:([0-9a-fA-F]+)(:[^:]*){6}:[^:]*[eE].*/\2/p'
gpg --homedir ${GNUPGHOME} -k
```

## export public keyring so that luks/initramfs can use it
`gpg --homedir ${GNUPGHOME} --export --export-options export-minimal > /etc/cryptsetup-initramfs/pubring.gpg`


## create key
This key will be encrypted and included in the initrmfs.  During boot, cryptsetup will use gpg to decrypt the key, and then pass it to luks.

`dd if=/dev/random bs=1 count=256 > ${TMPKEY}`


## encrypt key
`gpg -o /etc/cryptsetup-initramfs/cryptkey.gpg -r <key ID>  --encrypt ${TMPKEY}`

You can add multiple recipients by repeating the `-r` flag.

Use `-R` if you want to keep the recipient list anonymous.


## add key to luks
You will need to know an existing password to be able to add the new key.

`cryptsetup luksAddKey <blah disk> ${TMPKEY}`

We no longer need the key.  This might be overkill since these shouldn't survive a reboot.
```
shred ${TMPKEY}
rm ${TMPKEY}
rm -r ${TMPDIR}
```

## add hooks

cryptsetup uses these files to make sure that the initramfs has all the parts it needs.

```
cp cryptgnupg-sc-failsafe /etc/initramfs-tools/hooks/
chmod 755 /etc/initramfs-tools/hooks/cryptgnupg-sc-failsafe
````

There are some modifications here.  The source file for this is here /usr/share/initramfs-tools/hooks/cryptgnupg-sc


`cp -p /usr/share/initramfs-tools/scripts/local-bottom/cryptgnupg-sc /etc/initramfs-tools/scripts/local-bottom/cryptgnupg-sc-failsafe`

There is no changes to this file other than the file name, but update-initramfs will look for it.


## add script
```
cp decrypt_gnupg-sc-failsafe  /etc/cryptsetup-initramfs/
chmod 755  /etc/cryptsetup-initramfs/decrypt_gnupg-sc-failsafe
```

There are some modifications here so that it will look for the nitrokey/smartcard before falling back to askpass (and dropbear/ssh, if enabled).  The source file for this is here /usr/lib/cryptsetup/scripts/decrypt_gnupg-sc


## update /etc/crypttab to that luks can find it
```
<name> UUID=<blah> /etc/cryptsetup-initramfs/cryptkey.gpg luks,keyscript=/etc/cryptsetup-initramfs/decrypt_gnupg-sc-failsafe
```


# update initramfs
`update-initramfs -vuk all`


You can verify the changes.

`lsinitramfs /boot/initrd.img-X.X.X-X-amd64 | grep -e pubring.kbx -e cryptkey.gpg -e cryptgnupg -e decrypt -e pinentry`

```
cryptroot/gnupghome/pubring.kbx				<- this is the public keyring
etc/cryptsetup-initramfs/cryptkey.gpg			<- this is the encrypted key
etc/cryptsetup-initramfs/decrypt_gnupg-sc-failsafe	<- this is our executable
scripts/local-bottom/cryptgnupg-sc
scripts/local-bottom/cryptgnupg-sc-failsafe		<- this is our executable
usr/bin/pinentry
usr/bin/pinentry-tty					<- use pinentry-tty instead of pinentry-curses
```
